from typing import Union
from fastapi.middleware.cors import CORSMiddleware
from fastapi import FastAPI
from pydantic import BaseModel
import joblib
import sklearn
import numpy as np 
print('The scikit-learn version is {}.'.format(sklearn.__version__))
model1 = joblib.load('BodyFat.pkl')
app = FastAPI()

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


class Item(BaseModel):
    Weight : float
    Chest : float
    Abdomen : float
    Hip : float
    Thigh : float
    is_offer: Union[bool, None] = None

@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
    return {"item_id": item_id, "q": q}

@app.put("/items")
async def update_item(item: Item):
    data = np.array(
    [
     [item.Weight,item.Chest,item.Abdomen,item.Hip,item.Thigh]
    ])
    answer = model1.predict(data)
    print(answer[0])
    return {"Answer": answer[0]}